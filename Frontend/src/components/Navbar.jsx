import React from "react";
import { Nav, Button, Navbar, Container } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../redux/actions/authActions";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

import { whoAmI } from "../redux/actions/authActions";

function NavBar(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isAuthenticated, user } = useSelector((state) => state.auth);

  const handleLogout = () => {
    dispatch(logout());
    return navigate("/");
  };
  const alertLogout = async () => {
    await Swal.fire({
      title: "Are you sure?",
      text: "You will be logged out",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, logout!",
    });
    handleLogout();
  };

  setTimeout(() => {
    if (isAuthenticated) {
      dispatch(whoAmI());
    }
  }, 10000);

  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        className="bg-nav fw-bold"
        fixed="top"
      >
        <Container>
          <Navbar.Brand href="/cars">Binar Car Rental</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ms-auto">
              <Nav.Link href="/#ourservice" className="mx-3">
                Our Service
              </Nav.Link>
              <Nav.Link href="/#whyus" className="mx-3">
                Why Us
              </Nav.Link>
              <Nav.Link href="/#testimonial" className="mx-3">
                Testimonial
              </Nav.Link>
              <Nav.Link href="/#faq" className="mx-3">
                FAQ
              </Nav.Link>
              {!isAuthenticated ? (
                <Button variant="success" className="mx-3" href="/register">
                  Register
                </Button>
              ) : (
                <>
                  <Nav.Link className="mx-3">User {user}</Nav.Link>
                  <Button
                    variant="danger"
                    className="mx-3"
                    onClick={alertLogout}
                  >
                    Logout
                  </Button>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

export default NavBar;
