import { GET_ALL_CARS, CARS_ERROR, GET_FILTERED_CARS } from "./types";

export const getAllCars = () => async (dispatch) => {
  try {
    const response = await fetch("http://localhost:8000/api/v1/cars");
    const data = await response.json();
    console.log(data);

    dispatch({
      type: GET_ALL_CARS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: CARS_ERROR,
      payload: error.response,
    });
  }
};

export const getFilteredCars = (date, time, passenger) => async (dispatch) => {
  try {
    const response = await fetch(
      `http://localhost:8000/api/v1/cars/filter/${date}/${time}/${passenger}`
    );
    const data = await response.json();

    dispatch({
      type: GET_FILTERED_CARS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: CARS_ERROR,
      payload: error.response,
    });
  }
};
