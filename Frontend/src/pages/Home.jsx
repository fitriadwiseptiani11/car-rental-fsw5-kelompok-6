import { Accordion, Button } from "react-bootstrap";
import "../css/style.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { Link } from "react-router-dom";
import NavBar from "../components/Navbar";
import Footer from "../components/Footer";

import Mobil from "../img/mobil.png";
import { Container, Row, Col, Carousel } from "react-bootstrap";

function Home() {
  return (
    <div>
      {/* Navbar */}
      <div className="bg-nav">
        <NavBar />
      </div>

      {/* Header */}
      <div className="bg-head">
        <Container className="d-flex">
          <Row>
            <Col sm={12} md={5} lg={5} className="main-section-left">
              <h3>
                <strong>Sewa & Rental Mobil Terbaik di Kawasan Karawang</strong>
              </h3>
              <p>
                Selamat datang di Binar Car Rental. Kami menyediakan mobil
                kualitas terbaik dengan harga terjangkau. Selalu siap melayani
                kebutuhanmu untuk sewa mobil selama 24 jam.
              </p>
              <Link to="/cars">
                <Button variant="success" className="btn bg-btn">
                  Mulai Sewa Mobil
                </Button>
              </Link>
            </Col>
            <Col sm={12} md={7} lg={7} className="main-section-right">
              <img src={Mobil} className="img-fluid" alt="Car" />
            </Col>
          </Row>
        </Container>
      </div>

      {/* Our Service */}
      <div id="ourservice" className="container mt-5">
        <div className="row">
          <div className="col-sm grid-service grid-best">
            <img
              alt=""
              src="/images-landingpage/img_service.png"
              className="size-service"
            />
          </div>
          <div className="col-sm grid-service">
            <h3>
              <b>Best Car Rental for any kind of trip in Karawang!</b>
            </h3>
            <p>
              Sewa mobil di Karawang bersama Binar Car Rental jaminan harga
              lebih murah dibandingkan yang lain, kondisi mobil baru, serta
              kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
              wedding, meeting, dll.
            </p>
            <div className="d-flex flex-row">
              <div className="p-2">
                <img src="/images-landingpage/Group53.png" alt="" />
              </div>
              <div className="p-2">Sewa Mobil Dengan Supir di Bali 12 Jam</div>
            </div>
            <div className="d-flex flex-row">
              <div className="p-2">
                <img src="/images-landingpage/Group53.png" alt="" />
              </div>
              <div className="p-2">Sewa Mobil Lepas Kunci di Bali 24 Jam</div>
            </div>
            <div className="d-flex flex-row">
              <div className="p-2">
                <img src="/images-landingpage/Group53.png" alt="" />
              </div>
              <div className="p-2">Sewa Mobil Jangka Panjang Bulanan</div>
            </div>
            <div className="d-flex flex-row">
              <div className="p-2">
                <img src="/images-landingpage/Group53.png" alt="" />
              </div>
              <div className="p-2">Gratis Antar - Jemput Mobil di Bandara</div>
            </div>
            <div className="d-flex flex-row">
              <div className="p-2">
                <img src="/images-landingpage/Group53.png" alt="" />
              </div>
              <div className="p-2">Layanan Airport Transfer / Drop In Out</div>
            </div>
          </div>
        </div>
      </div>

      {/* Why Us */}
      <section id="whyus" className="grid-service">
        <div className="container mt-5">
          <div className="whyus?">
            <h3>
              <b>Why Us?</b>
            </h3>
            <p>Mengapa harus pilih Binar Car Rental?</p>
          </div>
          <div className="why-text">
            <div className="row">
              <div className="col-md ">
                <div className="card icon-promotion">
                  <div className="card-body">
                    <img alt="" src="/images-landingpage/icon_complete.png" />
                    <h6 style={{ marginTop: "10px" }}>
                      <b>Mobil Lengkap</b>
                    </h6>
                    <p>
                      Tersedia banyak pilihan mobil, kondisi masih baru, bersih
                      dan terawat
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md grid-service1">
                <div className="card icon-promotion">
                  <div className="card-body">
                    <img alt="" src="/images-landingpage/icon_price.png" />
                    <h6 style={{ marginTop: "10px" }}>
                      <b>Harga Murah</b>
                    </h6>
                    <p>
                      Harga murah dan bersaing, bisa bandingkan harga kami
                      dengan rental mobil lain
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md grid-service1">
                <div className="card icon-promotion">
                  <div className="card-body">
                    <img alt="" src="/images-landingpage/icon_24hrs.png" />
                    <h6 style={{ marginTop: "10px" }}>
                      <b>Layanan 24 Jam</b>
                    </h6>
                    <p>
                      Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami
                      juga tersedia di akhir minggu
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-md grid-service1">
                <div className="card icon-promotion">
                  <div className="card-body">
                    <img
                      alt=""
                      src="/images-landingpage/icon_professional.png"
                    />
                    <h6 style={{ marginTop: "10px" }}>
                      <b>Sopir Profesional</b>
                    </h6>
                    <p>
                      Sopir yang profesional, berpengalaman, jujur, ramah dan
                      selalu tepat waktu
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* Testimonial */}
      <section id="testimonial" className="py-5">
        <Container className="pt-4">
          <h2 className="h2 text-center">Testimonial</h2>
          <p className="text-center py-3">
            Berbagai review positif dari para pelanggan kami
          </p>
          <Carousel className="bg-testimonial">
            <Carousel.Item interval={2000}>
              <Row className="mx-4 my-5">
                <Col lg={2} className="m-auto">
                  <img
                    src="/images-landingpage/img_photo_(1).png"
                    className="d-block testimonial-img m-auto"
                    alt=""
                  />
                </Col>
                <Col lg={10}>
                  <img
                    src="/images-landingpage/Star.png"
                    className="testimonial-rating"
                    alt=""
                  />
                  <p>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p className="testimonial-name">John Dee 32, Bromo</p>
                </Col>
              </Row>
            </Carousel.Item>
            <Carousel.Item interval={2000}>
              <Row className="mx-4 my-5">
                <Col lg={2} className="m-auto">
                  <img
                    src="/images-landingpage/img_photo_(2).png"
                    className="d-block testimonial-img m-auto"
                    alt=""
                  />
                </Col>
                <Col lg={10}>
                  <img
                    src="/images-landingpage/Star.png"
                    className="testimonial-rating"
                    alt=""
                  />
                  <p>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p className="testimonial-name">John Dee 32, Bromo</p>
                </Col>
              </Row>
            </Carousel.Item>
            <Carousel.Item interval={2000}>
              <Row className="mx-4 my-5">
                <Col lg={2} className="m-auto">
                  <img
                    src="/images-landingpage/img_photo.png"
                    className="d-block testimonial-img m-auto"
                    alt=""
                  />
                </Col>
                <Col lg={10}>
                  <img
                    src="/images-landingpage/Star.png"
                    className="testimonial-rating"
                    alt=""
                  />
                  <p>
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
                    consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p className="testimonial-name">John Dee 32, Bromo</p>
                </Col>
              </Row>
            </Carousel.Item>
          </Carousel>
        </Container>
      </section>

      {/* Getting Started */}
      <div className="container grid-service">
        <div className="jumbotron grid-rentcar">
          <div className="row">
            <div className="col-sm text-center text-light">
              <h1 className="pb-3 pt-5">Sewa Mobil di Karawang Sekarang</h1>
              <p className="spacing-text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.{" "}
              </p>
              <Link to="/cars">
                <Button variant="success" className="btn bg-btn mt-5 mb-5">
                  Mulai Sewa Mobil
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>

      {/* FAQ */}
      <div id="faq" className="container grid-service grid-service1">
        <div className="row">
          <div className="col-sm faq">
            <h4>
              <b>Frequently Asked Question</b>
            </h4>
            <p className="pt-3">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit
            </p>
          </div>
          <div className="col-sm">
            <Accordion>
              <Accordion.Item eventKey="0">
                <Accordion.Header>
                  Apa saja syarat yang dibutuhkan untuk sewa mobil?
                </Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia
                  possimus voluptatum inventore quo impedit, quis eius rerum
                  pariatur dolor, vel sit esse est asperiores, nihil eligendi
                  nesciunt quam voluptas recusandae.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="1">
                <Accordion.Header>
                  Apakah persyaratan lepas kunci dan dengan supir berbeda?
                </Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ex
                  animi cupiditate delectus doloribus consectetur culpa, cum
                  nisi est quam qui officiis incidunt ab laudantium. Rerum quos
                  quis accusamus amet reprehenderit.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="2">
                <Accordion.Header>
                  Berapa hari sebelumnya sebaiknya booking sewa mobil?
                </Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Provident esse facere praesentium quidem necessitatibus,
                  repellendus nulla accusantium odio deleniti illum neque quod
                  repellat ullam in vel architecto voluptatem velit animi.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="3">
                <Accordion.Header>
                  Apakah ada biaya tambahan jika terlambat mengembalikkan mobil?
                </Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Sequi illum ad non saepe ducimus eligendi voluptates aut,
                  distinctio minima earum repudiandae enim delectus. Tenetur
                  obcaecati laborum commodi enim natus officia.
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="4">
                <Accordion.Header>
                  Bagaimana jika terjadi kerusakan pada mobil?
                </Accordion.Header>
                <Accordion.Body>
                  Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                  Cumque natus est amet mollitia sunt maiores nam quos iure
                  voluptate. Perferendis dolores eum quisquam quidem nisi nemo
                  minus aperiam illo obcaecati!
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </div>
        </div>
      </div>

      {/* footer */}
      <Footer />
    </div>
  );
}

export default Home;
